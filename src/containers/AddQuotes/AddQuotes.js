import React, {useState} from 'react';
import Header from "../../components/Header/Header";
import {CATEGORIES} from "../../ConstObjects";
import axiosUrl from "../../AxiosUrl";
import Spinner from "../../components/Spinner/Spinner";
import './AddQuotes.css';

const AddQuotes = props => {

    const [newQuote, setNewQuote] = useState({
        categories: 'friendship',
        author: '',
        text: ''
    });

    const [loading, setLoading] = useState(false);

    const changeQuote = e => {
        const {name, value} = e.target;

        setNewQuote(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const sendQuote = async event => {
        event.preventDefault();
        setLoading(true);

        try {
            const newQuoteCopy = {...newQuote};
            await axiosUrl.post('quotes.json', newQuoteCopy);
        } catch (e) {
            console.error(e);
        } finally {
            setLoading(false);
            props.history.push('/');
        }
    };

    let form = (
        <form onSubmit={sendQuote}>
            <p>Categories</p>
            <select onChange={e => {changeQuote(e)}} name="categories">
                {CATEGORIES.map(qCategories => (
                    <option key={qCategories} value={qCategories}>{qCategories}</option>
                ))}
            </select>
            <p>Author</p>
            <input
                type="text"
                name="author"
                value={newQuote.author}
                onChange={e => changeQuote(e)}
            />
            <p>Quote text</p>
            <textarea
                name="text"
                value={newQuote.text}
                onChange={e => changeQuote(e)}
            />
            <button>Save</button>
        </form>
    );

    if (loading) {
        form = <Spinner />;
    }

    return (
        <div className="AddQuotes">
            <Header changeUrl={props} />
            <p className="sub">Submit new quote</p>
            {form}
        </div>
    );
};

export default AddQuotes;