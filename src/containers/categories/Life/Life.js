import React from 'react';
import './Life.css';
import Header from "../../../components/Header/Header";
import SideBar from "../../../components/SideBar/SideBar";

const Life = props => {
    return (
        <div className="Life">
            <Header changeUrl={props} />
            <SideBar url={props} />
            this is Life
        </div>
    );
};

export default Life;