import React from 'react';
import Header from "../../../components/Header/Header";
import SideBar from "../../../components/SideBar/SideBar";

const Funny = props => {
    return (
        <div>
            <Header changeUrl={props} />
            <SideBar url={props} />
            This is Funny
        </div>
    );
};

export default Funny;