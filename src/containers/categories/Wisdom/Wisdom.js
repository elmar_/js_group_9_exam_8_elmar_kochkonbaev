import React from 'react';
import Header from "../../../components/Header/Header";
import SideBar from "../../../components/SideBar/SideBar";

const Wisdom = props => {
    return (
        <div>
            <Header changeUrl={props} />
            <SideBar url={props} />
            this is Wisdom
        </div>
    );
};

export default Wisdom;