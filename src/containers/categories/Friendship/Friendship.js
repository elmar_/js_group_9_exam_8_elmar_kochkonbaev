import React, {useEffect, useState} from 'react';
import Header from "../../../components/Header/Header";
import SideBar from "../../../components/SideBar/SideBar";
import axiosUrl from "../../../AxiosUrl";
import Spinner from "../../../components/Spinner/Spinner";
import Post from "../../../components/Post/Post";

const Friendship = props => {
    const [quotes, setQuotes] = useState([]);

    const [ids, setIds] = useState([]);

    useEffect(() => {
        const getPosts = async () => {
            const response = await axiosUrl.get('quotes.json?orderBy="categories"&equalTo="' + props.match.params.category + '"');
            setQuotes(Object.values(response.data).reverse());
            setIds(Object.keys(response.data).reverse());
        }
        getPosts().catch(console.error);
    }, [props.match.params.category])

    let blog = <Spinner />

    if (quotes.length > 0) {
        blog = (quotes.map((quote, i) => (
            <Post data={quote} id={ids[i]} url={props} />
        )));
    }

    return (
        <div>
            <Header changeUrl={props} />
            <SideBar url={props} />
            {blog}
        </div>
    );
};

export default Friendship;