import React, {useState, useEffect} from 'react';
import Header from "../../components/Header/Header";
import SideBar from "../../components/SideBar/SideBar";
import axiosUrl from "../../AxiosUrl";
import Post from "../../components/Post/Post";
import './AllQuotes.css';
import Spinner from "../../components/Spinner/Spinner";

const AllQuotes = props => {

    const [posts, setPosts] = useState([]);

    const [ids, setIds] = useState([]);

    useEffect(() => {
        const getPosts = async () => {
            const response = await axiosUrl.get('quotes.json');
            setPosts(Object.values(response.data).reverse());
            setIds(Object.keys(response.data).reverse());
        }
        getPosts().catch(console.error);
    }, [])

    let blog = <Spinner />

    if (posts.length > 0) {
        blog = (posts.map((post, i) => (
            <Post data={post} id={ids[i]} url={props} />
        )));
    }


    return (
        <div className="AllQuotes">
            <Header changeUrl={props}/>
            <SideBar url={props} />
            <h3>All</h3>
            {blog}
        </div>
    );
};

export default AllQuotes;