import React, {useState, useEffect} from 'react';
import Header from "../../components/Header/Header";
import axiosUrl from "../../AxiosUrl";
import {CATEGORIES} from "../../ConstObjects";
import Spinner from "../../components/Spinner/Spinner";
import './Edit.css';

const Edit = props => {

    const [quote, setQuote] = useState({});

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        const getQuote = async () => {
            setLoading(true);
            const response = await axiosUrl.get('quotes/' + props.match.params.id + '.json');
            setQuote(response.data);
        };

        getQuote().catch(console.error);
        setLoading(false);
    }, [props.match.params.id]);

    const changeQuote = e => {
        const {name, value} = e.target;

        setQuote(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const sendQuote = async event => {
        event.preventDefault();
        setLoading(true);

        try {
            const newQuoteCopy = {...quote};
            await axiosUrl.post('quotes.json', newQuoteCopy);
        } catch (e) {
            console.error(e);
        } finally {
            setLoading(false);
            props.history.push('/');
        }
    };

    let form = (
        <form onSubmit={sendQuote}>
            <p>Categories</p>
            <select value={quote.categories} onChange={e => {changeQuote(e)}} name="categories">
                {CATEGORIES.map(qCategories => (
                    <option key={qCategories} value={qCategories}>{qCategories}</option>
                ))}
            </select>
            <p>Author</p>
            <input
                type="text"
                name="author"
                value={quote.author}
                onChange={e => changeQuote(e)}
            />
            <p>Quote text</p>
            <textarea
                name="text"
                value={quote.text}
                onChange={e => changeQuote(e)}
            />
            <button>Save</button>
        </form>
    );

    if (loading) {
        form = <Spinner />;
    }

    return (
        <div className="Edit">
            <Header changeUrl={props} />
            <p className="sub">Edit quote</p>
            {form}
        </div>
    );
};

export default Edit;