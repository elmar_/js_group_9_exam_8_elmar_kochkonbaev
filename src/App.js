import React from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import AllQuotes from "./containers/AllQuotes/AllQuotes";
import Friendship from "./containers/categories/Friendship/Friendship";
import AddQuotes from "./containers/AddQuotes/AddQuotes";
import './App.css';
import Edit from "./containers/Edit/Edit";


const App = () => {
    return (
        <div className="container">
            <BrowserRouter>
                <Switch>
                    <Route path="/" exact component={AllQuotes} />
                    {/*<Route path="/:category" component={Friendship} />*/}
                    <Route path="/add-quotes" component={AddQuotes} />
                    <Route path="/edit/:id" component={Edit} />
                </Switch>
            </BrowserRouter>
        </div>
    );
};

export default App;