import axios from "axios";

const axiosUrl = axios.create({
    baseURL: 'https://classwork-63-burger-default-rtdb.firebaseio.com/'
});

export default axiosUrl;