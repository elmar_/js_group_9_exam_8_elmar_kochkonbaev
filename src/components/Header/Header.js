import React from 'react';
import './Header.css';

const Header = ({changeUrl}) => {

    const historyChange = place => {
        changeUrl.history.push({pathname: place});
    };

        return (
            <div className="Header">
                <a href="/">Quotes Central</a>
                <div>
                    <ul className="list">
                        <li onClick={() => {historyChange('/')}}>Quotes</li>
                        <li onClick={() => {historyChange('/add-quotes')}}>Submit new quote</li>
                    </ul>
                </div>
            </div>
        );
};

export default Header;