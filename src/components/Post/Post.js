import React from 'react';
import './Post.css';
import axiosUrl from "../../AxiosUrl";

const Post = ({data, id, url}) => {

    const removePost = () => {
        axiosUrl.delete('quotes/' + id + '.json').catch(console.error);
    }

    const editQuote = () => {
        url.history.push({pathname: '/edit/' + id});
    }

    return (
        <div className='Post'>
            <p className="author">{data.author}</p>
            <p className='text'>{data.text}</p>
            <div>
                <button className="btn" onClick={removePost}>Delete</button>
                <button className="btn" onClick={editQuote}>Edit</button>
            </div>
        </div>
    );
};

export default Post;