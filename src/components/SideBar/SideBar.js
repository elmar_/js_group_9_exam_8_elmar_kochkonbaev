import React from 'react';
import {CATEGORIES} from '../../ConstObjects';
import './SideBar.css';

const SideBar = ({url}) => {

    const historyChange = place => {
        url.history.push({pathname: '/' + place});
    };

    return (
        <div className="SideBar">
            <ul>
                <li onClick={() => {historyChange('/')}}>all</li>
                {CATEGORIES.map(qCategories => (
                    <li key={qCategories} onClick={() => {historyChange(qCategories)}}>{qCategories}</li>
                ))}
            </ul>
        </div>
    );
};

export default SideBar;